# matems

A reimplementation of popular utility [ms by vercel](https://github.com/vercel/ms/) for exercise purposes.

Description from the original repo: 
```
Use this package to easily convert various time formats to milliseconds.
```

## Usage

```js
matems('2 days')  // 172800000
matems('1d')      // 86400000
matems('10h')     // 36000000
matems('2.5 hrs') // 9000000
matems('2h')      // 7200000
matems('1m')      // 60000
matems('5s')      // 5000
matems('1y')      // 31557600000
matems('100')     // 100
matems('-3 days') // -259200000
matems('-1h')     // -3600000
matems('-200')    // -200
```

### Convert from Milliseconds

```js
matems(60000)             // "1m"
matems(2 * 60000)         // "2m"
matems(-3 * 60000)        // "-3m"
matems(matems('10 hours'))    // "10h"
```

### Time Format Written-Out

```js
matems(60000, { long: true })             // "1 minute"
matems(2 * 60000, { long: true })         // "2 minutes"
matems(-3 * 60000, { long: true })        // "-3 minutes"
matems(matems('10 hours'), { long: true })    // "10 hours"
```

## Tests

Run `npm test` to run mocha tests
