const unitsToSeconds = {
    'ms': 1, 'msecs': 1, 'milliseconds': 1,
    's': 1, 'sec': 1, 'secs':1, 'seconds': 1,
    'm': 60, 'min': 60, 'mins':60, 'minute':60, 'minutes':60,
    'h': 3600, 'hr': 3600, 'hour':3600, 'hours': 3600,
    'd': 86400, 'day': 86400, 'days': 86400,
    'w': 604800, 'week': 604800, 'weeks': 604800
};

function matems(inputTime, options = {}) {
    if ((typeof inputTime !== 'string' && typeof inputTime !== 'number') 
    || inputTime === '' || (typeof inputTime == 'number' && isNaN(inputTime)) || inputTime === Infinity || inputTime === -Infinity) {
        throw new Error(`Error: val is not a non-empty string or a valid number. val=${inputTime}`);
    }

    if (typeof inputTime === 'string') {
        inputTime = inputTime.trim();

        // find the start of the unit string
        for(var i = inputTime.length-1; i >= 0; i--) {
            const ch = inputTime[i];

            if(!isNaN(ch)) {
                i++; // the first non-digit character
                break;
            }
        }
        
        let value = inputTime.substring(0, i);
        let unit;
        if(i < inputTime.length)
            unit = inputTime.substr(i).trim().toLowerCase();
        else
            unit = 'ms';

        if (isNaN(value) || !unitsToSeconds[unit]) 
            return NaN;

        return convertToMs(parseFloat(value), unit);
    }
    else {
        const sign = (inputTime < 0) ? -1 : 1;
        
        let long = options['long'];

        let absVal = Math.abs(inputTime);
        let convertedVal;
        let unit = '';

        if(absVal < 1000) {
            convertedVal = absVal;

            unit = (long) ? ' ms' : 'ms';
        }
        else if(absVal < 60000) {
            convertedVal = Math.round(absVal / 1000);

            unit = (long) ? ' second' : 's';
            if(long && convertedVal > 1)
                unit = unit + 's';
        }
        else if(absVal < 3600000) {
            convertedVal = Math.round(absVal / 1000 / 60);

            unit = (long) ? ' minute' : 'm';
            if(long && convertedVal > 1)
                unit = unit + 's';
        }
        else if(absVal < 86400000) {
            convertedVal = Math.round(absVal / 1000 / 3600);

            unit = (long) ? ' hour' : 'h';
            if(long && convertedVal > 1)
                unit = unit + 's';
        }
        else {
            convertedVal = Math.round(absVal / 1000 / 86400);

            unit = (long) ? ' day' : 'd';
            if(long && convertedVal > 1)
                unit = unit + 's';
        }

        return (sign*convertedVal) + '' + unit;
    }
}

/* Convert parameter value from param unit to miliseconds
and return as number */
function convertToMs(value, unit) {
    if (unit === 'ms' || unit === 'msecs' || unit === 'milliseconds')
        return value;
    
    return value * unitsToSeconds[unit] * 1000;
}

// export as module if used by nodejs
if(typeof module !== 'undefined') {
    module.exports = matems;
}